FROM python:3.11-slim

RUN apt update && \
    apt install -y --no-install-recommends \
    build-essential libpq-dev vim sudo procps \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install python packages
# RUN pip3 install --upgrade pip setuptools
RUN pip3 install --upgrade pip setuptools && \
    pip3 install \
    # udm
    numpy==1.24.1 \
    matplotlib==3.6.2 \
    pandas==1.5.2 \
    black==22.12.0 \
    scikit-learn==1.2.0 \
    seaborn==0.12.1 \
    jupyterlab==3.5.2 \
    jupyterlab_code_formatter==1.5.3 \
    jupyterlab-git==0.41.0 \
    jupyterlab_widgets==3.0.5 \ 
    ipywidgets==8.0.4 \
    import-ipynb==0.1.4 \
    opencv-python==4.6.0.66 \
    opencv-contrib-python==4.6.0.66
    # lckr-jupyterlab-variableinspector==0.39.1 \


# Change language and timezone (UTC -> JST)
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
RUN ln -sf /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

# Create and switch user (Requirement: DOCKER_UID is the same as host uid)
ARG DOCKER_UID=1000
ARG DOCKER_USER=user
ARG DOCKER_PASSWORD=user
RUN useradd -m --uid ${DOCKER_UID} --groups sudo ${DOCKER_USER} \
    && echo ${DOCKER_USER}:${DOCKER_PASSWORD} | chpasswd \
    && usermod -a -G video ${DOCKER_USER}
USER ${DOCKER_USER}

WORKDIR /home/${DOCKER_USER}/app
COPY ./app/ /home/${DOCKER_USER}/app
